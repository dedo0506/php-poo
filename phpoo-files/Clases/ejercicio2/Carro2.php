<?php
//creación de la clase carro
class Carro2{

	//declaracion de propiedades
	public $color;
	public $modelo;

	//agregamos el ano de fabricacio y el atributo para definir el estado de circula el carro
	public $anioFab;
	private $circula;
	
	//declaracion del método verificación
	public function verificacion($anioFab){
		
		if($anioFab < 1990){
			
			$this->circula = "NO"; //le damos valor al atributo circula 
			
		}elseif($anioFab >= 1990 && $anioFab < 2010){
			
			$this->circula = "REVISION"; //le damos valor al atributo circula

		}else{
			
			$this->circula = "SI"; //le damos valor al atributo circula

		}	

		return $this->circula; //regresamos el atributo circula ya con el valor que se le dio
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anioFab=$_POST['anioFab'];
}





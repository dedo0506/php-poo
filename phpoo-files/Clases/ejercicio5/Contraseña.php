<?php  
//declaracion de clase token
	class Contraseña{
		//declaracion de atributos
		private $nombre;
		private $contrasena;
		//declaracion de metodo constructor
		public function __construct($nombre){
			$this->nombre=$nombre;
			$this->$contrasena=randmString();
		}

		//Funcion para definir la contraseña aleatoriamente
		function randomString() {
			$longitud = 4;
			$caracteres = 'ABCDEFGHIJKLMNROPQRSTUVWXYZ';
			$random = substr(str_shuffle($caracteres), 0, $longitud);
			return $random;
		}


		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function mostrar(){
			return 'Hola '.$this->nombre.' este es la contraseña que se te genero: '.$this->contrasena;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destrucion del token
			$this->contrasena='El token ha sido destruido';
			echo $this->contrasena;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$contrasena1= new Contraseña($_POST['nombre']);
	//asignación del valor aleatorio a la contraseña del objeto
	$contrasena=$contrasena1->randomString();


	$mensaje=$contrasena1->mostrar();
}


?>
